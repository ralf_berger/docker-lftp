.PHONY: build

build:
	docker build -t regreb/lftp .
	docker tag regreb/lftp regreb/lftp:latest
