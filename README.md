# lftp FTP client

## Usage

```sh
docker run regreb/lftp [...]
```


## lftp help

```sh
docker run regreb/lftp
```


## Usage in GitLab deployment

Set the following variables:
* `$FTP_SERVER`: Host name or IP (without protocol)
* `$FTP_SERVER_DIR`: The target directory (e.g.: `/`)
* `$FTP_USER`
* `$FTP_PASS`


## Building

```sh
make
```
